# Lowest Common Ancestor - SWENG 19

This is a python implementation of the Lowest Common Ancestor problem. My chosen unit testing framework will be 'pytest'

In order to run the tests with code coverage and to generate a html version of the code coverage, run the following code

```python
py.test --cov LCATest
coverage html
```